#ifndef ENUMS_HPP
#define ENUMS_HPP

enum class InitShape
{
    sphere,
    plane,
    paraboloid,
    ellipsoid
};

#endif // ENUMS_HPP
